const GroupsHelper = require('@tanooki/gl.cli.js/groups.helper').Helper
const ProjectsHelper = require('@tanooki/gl.cli.js/projects.helper').Helper
const RepositoriesHelper = require('@tanooki/gl.cli.js/repositories.helper').Helper
/*
    # need: GITLAB_TOKEN_ADMIN
    node generate.content.js \
    "$STARTING_GROUP" \
    "$STARTING_WORKSHOP_NAME" \
    "$TARGET_GROUP" \
    "$TARGET_WORKSHOP_NAME" \
    "$PROJECTS_LIST_TO_COPY"
*/

async function exportProject({projectPath}) {
  try {
    let schedule = await ProjectsHelper.scheduleProjectExport({project: projectPath})
    console.log(schedule)
    console.log(schedule.get("message")) // should be equal to '202 Accepted'
  } catch (error) {
    console.log("😡 exportProject:", error.message)
    console.log("🖐️ parameters:", {projectPath})
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
}

async function checkProjectExport({projectPath}) {
  try {
    let projectExport = await ProjectsHelper.getProjectExportStatus({project: projectPath})
    console.log(projectExport.get("id"), projectExport.get("name"), projectExport.get("export_status"))
    return projectExport.get("export_status")
  } catch (error) {
    console.log("😡 checkProjectExport:", error.message)
    console.log("🖐️ parameters:", {projectPath})
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
}

async function downloadHandsonProject({projectPath, projectName}) {

  try {
    let downloadResult = await ProjectsHelper.downloadProjectExport({project: projectPath, file:__dirname+"/"+projectName+".gz"})
    console.log(downloadResult) // == true if all is ok
  } catch (error) {
    console.log("😡 downloadHandsonProject:", error.message)
    console.log("🖐️ parameters:", {projectPath, projectName})
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
}

// Create the handson project in the target workshop group
async function importHansonProject({targetGroup, targetWorkshopName, projectName}) {

  try {

    let importResult = await ProjectsHelper.importProjectFile({
      file: __dirname+"/"+projectName+".gz",
      namespace: `${targetGroup}/${targetWorkshopName}`,
      path: projectName
      //path: projectPath
    })

  } catch (error) {
    console.log("😡 importHansonProject:", error.message)
    console.log("🖐️ parameters:", {targetGroup, targetWorkshopName, projectName})
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
}

async function generate_content({startingGroup, startingWorkshopName, targetGroup, targetWorkshopName, projectsListToCopy}) {

  try {
    //TARGET_GROUP:
    //  value: "mjcc-workshops/workshops"
    let parentGroup = await GroupsHelper.getGroupDetails({group:`${targetGroup}`})
    let parent_group_id = parentGroup.get("id")
    console.log("👋", parent_group_id)
    // Create a group for the new workshop contents
    let newGroup = await GroupsHelper.createSubGroup({
      parentGroupId: parent_group_id,
      subGroupPath: `${targetWorkshopName}`,
      subGroupName: `${targetWorkshopName}`,
      visibility: "public"
    })
    console.log("📝", newGroup)

    let projectsList = projectsListToCopy.split(",").map(project => project.trim())

    // schedule export of the projects
    try {
      console.log("🎃", projectsList)
      for(var member in projectsList) {
        let projectName = projectsList[member]
        console.log("🎃", projectName)
        // *** schedule export of the codelab project ***
        await exportProject({projectPath: startingGroup+"/"+startingWorkshopName+"/"+projectName})
        // *** be sure that the export is available
        var condition = false
        while (condition==false) {
          let exportStatus = await checkProjectExport({projectPath: startingGroup+"/"+startingWorkshopName+"/"+projectName})
          console.log("👋", exportStatus)
          if(exportStatus=="finished") condition = true
        }
        // Download the handson project
        await downloadHandsonProject({projectPath: startingGroup+"/"+startingWorkshopName+"/"+projectName, projectName})

      }
    } catch(error) {
      console.log("😡 generate_content [schedule export and download]:", error.message)
      console.log("🖐️ parameters:", {startingGroup, startingWorkshopName, targetGroup, targetWorkshopName, projectsListToCopy})
      console.log(error.response.status, error.response.statusText)
      console.log(error.response.data)
    }

    // import project
    try {
      //importHansonProject({targetGroup, targetWorkshopName, projectName})
      console.log("🎃", projectsList)
      for(var member in projectsList) {
        let projectName = projectsList[member]
        console.log("🎃", projectName)

        // Create the handson project in the group of the workshop
        await importHansonProject({
          targetGroup,
          targetWorkshopName,
          projectName: projectName
        })
      }

    } catch(error) {
      console.log("😡 generate_content [import]:", error.message)
      console.log("🖐️ parameters:", {startingGroup, startingWorkshopName, targetGroup, targetWorkshopName, projectsListToCopy})
      console.log(error.response.status, error.response.statusText)
      console.log(error.response.data)
    }




  } catch(error) {
      console.log("😡 generate_content:", error.message)
      console.log("🖐️ parameters:", {startingGroup, startingWorkshopName, targetGroup, targetWorkshopName, projectsListToCopy})
      console.log(error.response.status, error.response.statusText)
      console.log(error.response.data)
  }

}


generate_content({
  startingGroup: process.argv[2],
  startingWorkshopName: process.argv[3],
  targetGroup: process.argv[4],
  targetWorkshopName: process.argv[5],
  projectsListToCopy: process.argv[6]
})



